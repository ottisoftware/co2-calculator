//
//  TotalCo2Chart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class Co2TotalChart: CollectionViewChart {
    
    override func render() -> LineChartView {
        return LineChart(frame: frame, yPoints: yValues).chart()
    }
    
    override func getYValues() -> [Double] {
        let results = self.getData()
        print("In DB: %@", results)
        let totalArrayByDay = self.getCo2perDay(items: results)
        print("Total: %f", totalArrayByDay)
        return totalArrayByDay
    }
    
    
    //Returns a list of co2 values per day
    func getCo2perDay(items: [TransportItem]) -> [Double] {
        var co2Array : [Double]!
        co2Array = []
        
        var lastDay = ""
        var i = 0
        for item in items  {
            let transport = TransportOption.initWithItem(item: item)
            let itemDay = transport.getCreateDate()
            if(itemDay == lastDay){
                co2Array[i-1] += transport.getCo2Impact()
            } else {
                lastDay = itemDay
                i += 1
                co2Array.append(transport.getCo2Impact())
            }
        }
        return co2Array
    }
}
