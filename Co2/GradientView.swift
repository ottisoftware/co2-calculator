//
//  HeaderView.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class GradientView: NSObject {
    private var view: UIView!
    
    init(view: UIView) {
        self.view = view
    }
    
    func withGradient() -> UIView {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        
        let blue = UIColor(red:33.0/255.0, green:145.0/255.0, blue:184.0/255.0, alpha:1.0)
        let green = UIColor(red:175.0/255.0, green:221.0/255.0, blue:178.0/255.0, alpha:1.0)
        gradientLayer.colors = [blue.cgColor, green.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        let path = UIBezierPath(roundedRect:self.view.bounds,
                                byRoundingCorners:[.bottomLeft, .bottomRight],
                                cornerRadii: CGSize(width: 30, height:  30))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.view.layer.mask = maskLayer
        
        return self.view
    }
    
}
