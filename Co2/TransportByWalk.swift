//
//  TransportByWalk.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByWalk: TransportOption {
    
    convenience required init() {
        self.init(name: "Walk")
        isBad = false
        co2perKm = 0.1
        icon = "🏃🏽‍♀️"
    }
}
