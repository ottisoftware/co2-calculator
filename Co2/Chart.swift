//
//  Chart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class Chart: NSObject {
    let frame : CGRect!
    let yPoints : [Double]!
    
    init(frame: CGRect, yPoints: [Double]) {
        self.frame = frame
        self.yPoints = yPoints
    }
    
}
