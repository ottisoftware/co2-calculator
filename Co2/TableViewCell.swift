//
//  TableViewCell.swift
//  Co2
//
//  Created by Tobias Wittekindt on 06.05.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var roundCell: UIView!
    
    @IBOutlet weak var disclosureImage: UIImageView!
    @IBOutlet weak var descriptionL: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundCell.layer.cornerRadius = 10;
        roundCell.layer.masksToBounds = false;
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisclosureImage(image: UIImage){
       self.disclosureImage.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.disclosureImage.image = image
        });
    }

}
