//
//  Co2TodayChart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class Co2TodayChart: CollectionViewChart {

    override func render() -> LineChartView {
        return LineChart(frame: frame, yPoints: yValues).chart()
    }
    
    override func getYValues() -> [Double] {
        let results = self.getData()
        var yValues : [Double] = []
        var cur = 0.0
        for item in results {
            let transport = TransportOption.initWithItem(item: item)
            cur += transport.getCo2Impact()
            yValues.append(cur)
        }

        return yValues
    }
    
}
