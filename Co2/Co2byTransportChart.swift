//
//  Co2byTransportChart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class Co2byTransportChart: CollectionViewChart {
    
    override func render() -> PieChartView {
        return PieChart(frame: frame, yPoints: yValues, labels: labels).chart()
    }
    
    override func getYValues() -> [Double] {
        let results = self.getData()
        var dict : Dictionary<String, Double>! = [:]
        for item in results {
            let transport = TransportOption.initWithItem(item: item)
            let name = transport.getIcon()
            
            dict.updateValue(item.totalCo2, forKey: name)
            
        }
        
        self.labels = Array(dict.keys)
        return Array(dict.values)
    }
    
    
    
}

