//
//  TransportByPlane.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByPlane: TransportOption {
    
    convenience required init() {
        self.init(name: "Plane")
        isBad = true
        co2perKm = 0.1
        icon = "✈️"
    }
}
