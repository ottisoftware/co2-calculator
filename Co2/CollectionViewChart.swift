//
//  CollectionViewChart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class CollectionViewChart: NSObject {
    var yValues : [Double]!
    var from : Date!
    var to : Date!
    var frame : CGRect!
    var labels : [String]!
    
    
    
    init(frame: CGRect, from: Date, to: Date) {
        super.init()
        self.from = from
        self.to = to
        self.frame = frame
        self.yValues = self.getYValues()
    }
    
    func render() -> UIView {
        return UIView()
    }
    
    //Loads Data from the DB
    func getData() -> [TransportItem] {
        let filter = NSPredicate(format: "created >= %@ AND created <= %@ ", from as CVarArg, to as CVarArg)
        
        return DBManager.sharedInstance.filterDB(filter: filter)
    }
    
    
    /*func totalYPointsValue() -> Double {
        var total = 0.0;
        for value in yValues {
            total += value
        }
        return total
    }*/
    func totalYPointsValue() -> Double {
        let results = self.getData()
        var total = 0.0;
        for item in results {
            let transport = TransportOption.initWithItem(item: item)
            total += transport.getCo2Impact()
        }
        return total
    }
    
    //Overide this function in the subclass
    func getYValues() -> [Double] {
        return [0.0]
    }
    
}
