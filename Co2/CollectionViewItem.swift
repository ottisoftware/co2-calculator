//
//  CollectionViewItem.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class CollectionViewItem: NSObject {
    let identifier : String!
    
    
    init(id: String) {
        self.identifier = id
    }
    
    func getTitle() -> String {
        switch identifier {
            case "totalCo2":
                return "TOTAL"
            case "co2byTransport":
                return "BY TRANSPORT"
            case "co2Today":
                return "TODAY"
            default:
                return ""
        }
    }
    
    func getChart(frame: CGRect, from: Date, to: Date) -> CollectionViewChart {
        switch identifier {
            case "totalCo2":
                return Co2TotalChart(frame: frame, from: from, to: to)
            case "co2byTransport":
                return Co2byTransportChart(frame: frame, from: from, to: to)
            case "co2Today":
                return Co2TodayChart(frame: frame, from: from, to: to)
            default:
                return Co2TotalChart(frame: frame, from: from, to: to)
        }
       
    }
    
}
