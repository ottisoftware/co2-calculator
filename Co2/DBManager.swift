//
//  DBmanager.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager: NSObject {

    private var   database:Realm
    
    static let   sharedInstance = DBManager()
    
    private override init() {
        database = try! Realm()
    }
    
    func getDataFromDB() ->   Results<TransportItem> {
        let results: Results<TransportItem> =   database.objects(TransportItem.self)
        return results
    }
    
    //Search the DB
    func filterDB(filter: NSPredicate) -> [TransportItem] {
        let results: Results<TransportItem> =   database.objects(TransportItem.self).filter(filter)
        
        let resultArray = Array(results)
        
        return resultArray
    }
    
    func addData(object: TransportItem)   {
        try! database.write {
            //Update ID
            object.ID = DBManager.sharedInstance.getDataFromDB().count
            database.add(object, update: true)
            print("Added new object")
        }
    }
    
    func deleteAllFromDatabase()  {
        try!   database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: TransportItem)   {
        try!   database.write {
            database.delete(object)
        }
    }
}
