//
//  TransportByCar.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByCar: TransportOption {

    convenience required init() {
        self.init(name: "Car")
        isBad = true
        co2perKm = 0.120
        icon = "🚗"
    }
    
    
}
