//
//  QuizItem.swift
//  Co2
//
//  Created by Tobias Wittekindt on 06.05.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class QuizItem: NSObject {
    
    let question : String
    let answers : [QuizAnswer]
    
    init(question: String, answers: [QuizAnswer]) {
        self.question = question
        self.answers = answers.shuffled()
    }
}
