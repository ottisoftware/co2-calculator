//
//  PieChart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class PieChart: NSObject {
    let frame : CGRect!
    let yPoints : [Double]!
    let labels : [String]!
    
    init(frame: CGRect, yPoints: [Double], labels: [String]) {
        self.frame = frame
        self.yPoints = yPoints
        self.labels = labels
    }
    
    func chart() -> PieChartView {
        let pieChart = PieChartView(frame: frame)
        pieChart.highlightPerTapEnabled = false
        initChartUI(chart: pieChart)
        pieChart.data = getChartData(values: yPoints, labels: labels)
        return pieChart
    }
    
    //Returns a pie Chart with certain points
    private func getChartData(values: [Double], labels: [String]) -> ChartData {
        var pieChartEntry = [PieChartDataEntry]()
        for i in 0..<values.count {
            let chartValue = PieChartDataEntry(value: values[i], label: labels[i])
            pieChartEntry.append(chartValue)
        }
        
        let pie = PieChartDataSet(entries: pieChartEntry, label: "")

        let green = UIColor(displayP3Red: 253.0/255.0, green: 159.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        let orange = UIColor(displayP3Red: 173.0/255.0, green: 218.0/255.0, blue: 189.0/255.0, alpha: 1.0)
        
        let lightRed = UIColor(displayP3Red: 251.0/255.0, green: 162.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        let lila = UIColor(displayP3Red: 148.0/255.0, green: 134.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    
        pie.colors = [green, orange, lightRed, lila]
        pie.entryLabelColor = .gray
        pie.drawValuesEnabled = false
        
        let chartData = PieChartData()
        chartData.addDataSet(pie)
        
        return chartData
    }
    
    func initChartUI (chart: PieChartView) {
        chart.legend.enabled = false
        //var p = PieChartView()
        chart.holeRadiusPercent = 0.9
    
    }
    
}
