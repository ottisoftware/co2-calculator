//
//  TransportByBike.swift
//  Co2
//
//  Created by Tobias Wittekindt on 19.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByBike: TransportOption {

    convenience required init() {
        self.init(name: "Bike")
        isBad = false
        co2perKm = 0.1
        icon = "🚲"
    }
}
