//
//  QuizAnswer.swift
//  Co2
//
//  Created by Tobias Wittekindt on 06.05.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class QuizAnswer: NSObject {

    var answer : String
    var isRight : Bool
    
    init(answer: String, isRight: Bool) {
        self.answer = answer
        self.isRight = isRight
    }
}
