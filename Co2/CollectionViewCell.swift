//
//  CollectionViewCell.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var subCell: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var labelOverImage: UILabel!
    @IBOutlet weak var titleUnderImage: UILabel!
    @IBOutlet weak var smallCellImage: UIImageView!
}
