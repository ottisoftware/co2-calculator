//
//  TransportByBus.swift
//  Co2
//
//  Created by Tobias Wittekindt on 19.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByBus: TransportOption {

    convenience required init() {
        self.init(name: "Bus")
        isBad = true
        co2perKm = 0.07
        icon = "🚌"
    }
}
