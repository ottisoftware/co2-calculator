//
//  TransportItem.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import RealmSwift

// We store this item in our Realm DB

class TransportItem: Object {
    @objc dynamic var ID = 0
    
    @objc dynamic var name : String! = ""
    @objc dynamic var distance = 0.0
    @objc dynamic var isBad = false
    @objc dynamic var co2perKm = 0.0
    @objc dynamic var icon : String! = ""
    @objc dynamic var image : String! = ""
    @objc dynamic var created = Date()
    @objc dynamic var totalCo2 = 0.0
    
    override static func primaryKey() -> String? {
        return "ID"
    }
}
