//
//  AddData.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import RealmSwift

class AddData: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    @IBOutlet weak var transportCollectionView: UICollectionView!
    @IBOutlet weak var distanceTextField: UITextField!
    @IBOutlet weak var headerView: UIView!
    var transportOptionsArray : [TransportOption]!
    
    //Collection View
    private let reuseIdentifier = "transportCell"
    private let itemsPerRow: CGFloat = 4
    private let sectionInsets = UIEdgeInsets(top: 20.0, left: 10.0, bottom: 20.0, right: 10.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariables()
        initUI()
        
        // Do any additional setup after loading the view.
    }
    
    func initVariables() {
        transportOptionsArray = [TransportByWalk(), TransportByBike(), TransportByTrain(), TransportByBus(), TransportByScooter(), TransportByCar(), TransportByPlane(), TransportByRocket()]
        self.transportCollectionView.delegate = self
        self.transportCollectionView.dataSource = self
    }
    
    func initUI(){
        self.headerView = GradientView(view: self.headerView).withGradient()
        self.distanceTextField.becomeFirstResponder()
        self.transportCollectionView.reloadData()
    }
    
    func initRecoginizers() {
        let tap = UITapGestureRecognizer(target: self, action: Selector(("selectedView")))
        tap.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap)
    }

    
    func selectedView() {
        //self.view.endEditing(true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

    
    //MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return transportOptionsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
  
        let cw = cell.frame.width
        let imageFrame = CGRect(x: 10, y: 0, width: cw-20, height: cw-20)
        cell.smallCellImage.frame = imageFrame
        cell.labelOverImage.frame = imageFrame
        cell.titleUnderImage.frame = CGRect(x: 0, y: cw-20, width: cw, height: 20)
        cell.smallCellImage.layer.masksToBounds = false
        cell.smallCellImage.layer.cornerRadius = cell.smallCellImage.frame.width/2
        cell.smallCellImage.clipsToBounds = true
        
        let transport = self.transportOptionsArray[indexPath.row]
        
        cell.labelOverImage.text = transport.getIcon()
        cell.titleUnderImage.text = transport.getName()
        cell.smallCellImage.image = transport.getImage()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
    
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selected = transportOptionsArray[indexPath.row]
        let dist = self.distanceTextField.text!
        if(dist == ""){
            return
        }
        selected.distance = Double(dist)
        storeInDB(transport: selected)
    }
    
    
    func storeInDB(transport: TransportOption){
        DBManager.sharedInstance.addData(object: transport.getItem())
        print(DBManager.sharedInstance.getDataFromDB().count)
        _ = navigationController?.popToRootViewController(animated: true)
        
    }
}
