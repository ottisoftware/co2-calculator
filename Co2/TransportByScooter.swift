//
//  TransportByScooter.swift
//  Co2
//
//  Created by Tobias Wittekindt on 19.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByScooter: TransportOption {

    convenience required init() {
        self.init(name: "Scooter")
        isBad = true
        co2perKm = 0.077
        icon = "🛵"
    }
}
