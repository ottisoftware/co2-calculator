//
//  TransportByTrain.swift
//  Co2
//
//  Created by Tobias Wittekindt on 19.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByTrain: TransportOption {

    convenience required init() {
        self.init(name: "Train")
        isBad = true
        co2perKm = 0.03
        icon = "🚂"
    }
}
