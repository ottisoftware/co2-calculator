//
//  LineChart.swift
//  Co2
//
//  Created by Tobias Wittekindt on 18.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts

class LineChart: NSObject {
    let frame : CGRect!
    let yPoints : [Double]!
    
    init(frame: CGRect, yPoints: [Double]) {
        self.frame = frame
        self.yPoints = yPoints
    }
    
    func chart() -> LineChartView {
        let lineChart = LineChartView(frame: frame)
        lineChart.pinchZoomEnabled = false
        lineChart.doubleTapToZoomEnabled = false
        initChartUI(chart: lineChart)
        lineChart.data = getChartData(YPoints: yPoints)
        return lineChart
    }

    
    //Returns a line Chart with certain points
    private func getChartData(YPoints: [Double]) -> ChartData {
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<YPoints.count {
            let chartValue = ChartDataEntry(x: Double(i), y:YPoints[i])
            lineChartEntry.append(chartValue)
            
        }
        
        let line = LineChartDataSet(entries: lineChartEntry, label: "")
        line.colors = [NSUIColor.orange];
        
        line.drawCirclesEnabled = false
        line.drawVerticalHighlightIndicatorEnabled = false
        line.drawValuesEnabled = false
        line.highlightColor = NSUIColor.orange
        line.lineWidth = 3.5
        line.mode = LineChartDataSet.Mode.cubicBezier
        line.drawVerticalHighlightIndicatorEnabled = false
        line.drawHorizontalHighlightIndicatorEnabled = false
        
        let chartData = LineChartData()
        chartData.addDataSet(line)
        
        
        return chartData
    }
    
    func initChartUI (chart: AnyObject) {
        let xAxis = chart.xAxis
        let lAxis = chart.leftAxis
        let rAxis = chart.rightAxis
        
        chart.legend.enabled = false
        
        xAxis!.drawLabelsEnabled = false
        xAxis!.drawGridLinesEnabled = false
        xAxis!.drawAxisLineEnabled = false
        xAxis!.drawLimitLinesBehindDataEnabled = false
        
        lAxis!.drawLabelsEnabled = false
        lAxis!.drawGridLinesEnabled = false
        lAxis!.drawAxisLineEnabled = false
        lAxis!.drawLimitLinesBehindDataEnabled = false
        
        rAxis!.drawLabelsEnabled = false
        rAxis!.drawGridLinesEnabled = false
        rAxis!.drawAxisLineEnabled = false
        rAxis!.drawLimitLinesBehindDataEnabled = false
        
    }
}
