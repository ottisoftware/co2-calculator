//
//  Quiz.swift
//  Co2
//
//  Created by Tobias Wittekindt on 06.05.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class Quiz: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var quizTableView: UITableView!
    
    @IBOutlet weak var pointsLabel: UILabel!
    var currentQuizItem : QuizItem!
    var questions : [QuizItem] = []
    var quizCount : Int = 0
    var totalPoints : Int = 0
    var firstAnswerTry : Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questions = [
            QuizItem(question: "How much greenhouse gas emissions are created by an electric vehicle (1Km)?",
                     answers: [
                        QuizAnswer(answer: "38g", isRight: true),
                        QuizAnswer(answer: "100g", isRight: false),
                        QuizAnswer(answer: "20g", isRight: false)
                ]
            ),
            QuizItem(question: "How much greenhouse gas emissions are created by a car (1Km)?",
                     answers: [
                        QuizAnswer(answer: "120g", isRight: true),
                        QuizAnswer(answer: "430g", isRight: false),
                        QuizAnswer(answer: "270g", isRight: false)
                ]
            ),
            QuizItem(question: "How much greenhouse gas emissions are created by a plane  (1Km)?",
                     answers: [
                        QuizAnswer(answer: "100g", isRight: true),
                        QuizAnswer(answer: "430g", isRight: false),
                        QuizAnswer(answer: "177g", isRight: false)
                ]
            ),
            QuizItem(question: "Cars emissions are responsible for how much of the total EU greenhouse gas emissions? ",
                     answers: [
                        QuizAnswer(answer: "20%", isRight: true),
                        QuizAnswer(answer: "25%", isRight: false),
                        QuizAnswer(answer: "10%", isRight: false),
                        QuizAnswer(answer: "30%", isRight: false)
                ]
            ),
            QuizItem(question: "How much green gas emissions do we produce daily by breathing? ",
                     answers: [
                        QuizAnswer(answer: "900g", isRight: true),
                        QuizAnswer(answer: "100g", isRight: false),
                        QuizAnswer(answer: "1Kg", isRight: false),
                        QuizAnswer(answer: "500g", isRight: false)
                ]
            ),
            QuizItem(question: "How much green gas emissions are absorbed by a tree daily? ",
                     answers: [
                        QuizAnswer(answer: "95g", isRight: true),
                        QuizAnswer(answer: "210g", isRight: false),
                        QuizAnswer(answer: "1Kg", isRight: false),
                        QuizAnswer(answer: "730g", isRight: false)
                ]
            ),
            QuizItem(question: "What is a cool fact about carbon dioxide? ",
                     answers: [
                        QuizAnswer(answer: "Fire extinguishers use it to put out fires.", isRight: true),
                        QuizAnswer(answer: "We can use it as fuel.", isRight: false),
                        QuizAnswer(answer: "Airplanes use it to fly.", isRight: false),
                        QuizAnswer(answer: "It dissolves itself over time.", isRight: false)
                ]
            ),
            QuizItem(question: "Who is the largest Co2 emitter? ",
                     answers: [
                        QuizAnswer(answer: "China", isRight: true),
                        QuizAnswer(answer: "USA", isRight: false),
                        QuizAnswer(answer: "India", isRight: false),
                        QuizAnswer(answer: "Germany", isRight: false)
                ]
            ),
            QuizItem(question: "How much Co2 is used to create 1kg of Lamb? ",
                     answers: [
                        QuizAnswer(answer: "39Kg", isRight: true),
                        QuizAnswer(answer: "11Kg", isRight: false),
                        QuizAnswer(answer: "20Kg", isRight: false),
                        QuizAnswer(answer: "450g", isRight: false)
                ]
            ),
            QuizItem(question: "How much carbon dioxide is produced to produce an iPhone? ",
                     answers: [
                        QuizAnswer(answer: "79Kg", isRight: true),
                        QuizAnswer(answer: "51Kg", isRight: false),
                        QuizAnswer(answer: "530Kg", isRight: false),
                        QuizAnswer(answer: "277Kg", isRight: false)
                ]
            )
        ].shuffled()
        
        currentQuizItem = questions[quizCount]
        // Do any additional setup after loading the view.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        } else {
            return currentQuizItem.answers.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let normalCell : NormalCell
        
        if(indexPath.section == 0){
            //The Question
            normalCell = NormalCell(title: "QUIZ", description: currentQuizItem.question)
        } else {
            //The Answers
            
            let title = String.init(format: "Answer %i", indexPath.row)
            normalCell = NormalCell(title: title, description: currentQuizItem.answers[indexPath.row].answer)
        }
        
        return normalCell.getCell(cell: cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            let indexPath = tableView.indexPathForSelectedRow
            let currentCell = tableView.cellForRow(at: indexPath!) as! TableViewCell
            let selectedAnswer = currentQuizItem.answers[indexPath!.row]
            if(selectedAnswer.isRight){
                currentCell.setDisclosureImage(image: UIImage.init(named: "success")!)
                if(firstAnswerTry) {
                    totalPoints += 1
                }
                showNextQuiz()
            } else {
                currentCell.setDisclosureImage(image: UIImage.init(named: "error")!)
                firstAnswerTry = false
            }
            updatePointLabel()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 150.0;
        } else {
            return 100.0;
        }
    }
    
    func showNextQuiz() {
        if(self.quizCount+1 == self.questions.count){
            showResults()
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.firstAnswerTry = true
            self.quizCount += 1
            self.currentQuizItem = self.questions[self.quizCount]
            self.quizTableView.reloadData()
        })
    }
    
    func updatePointLabel() {
        self.pointsLabel.text = String.init(format: "%i Points", self.totalPoints)
    }
    
    func showResults() {
        let result = String.init(format: "You made %i points. Know go and get your self an 🍦", self.totalPoints)
        let alert = UIAlertController(title: "You are awesome ❤️", message: result, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Nice", style: .default, handler: { (UIAlertAction) in
            _ = self.navigationController?.popViewController(animated: true)
        }));
        
        self.present(alert, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
