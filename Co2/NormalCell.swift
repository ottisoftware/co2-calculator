//
//  CellItem.swift
//  Co2
//
//  Created by Tobias Wittekindt on 06.05.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class NormalCell: NSObject {
    var title : String
    var descriptionText : String
    var segueIdentifier : String = ""
    var image : UIImage!
    
     init(title: String, description: String) {
        self.title = title
        self.descriptionText = description
        super.init()
        
        self.setSegue()
    }
    
    func setSegue() {
        self.segueIdentifier = "quiz"
    }
    
    func getCell(cell: TableViewCell) -> TableViewCell {
        cell.backgroundColor = .clear
        cell.title.text = self.title
        cell.descriptionL.text = self.descriptionText
        cell.disclosureImage?.isHidden = true
        return cell
    }
    
    func setImage(image: UIImage) {
        self.image = image
    }
    
    
}
