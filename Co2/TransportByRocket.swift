//
//  TransportByRocket.swift
//  Co2
//
//  Created by Tobias Wittekindt on 19.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportByRocket: TransportOption {
    convenience required init() {
        self.init(name: "Rocket")
        isBad = true
        co2perKm = 1000
        icon = "🚀"
    }
}
