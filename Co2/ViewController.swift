//
//  ViewController.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit
import Charts
import RealmSwift

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, ChartViewDelegate, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var addB: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var chartCollectionView: UICollectionView!
    
    //Collection View
    private let reuseIdentifier = "chartCell"
    private let itemsPerRow: CGFloat = 1
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 30.0, bottom: 50.0, right: 30.0)
    private var collectionItems : [CollectionViewItem]!
    private var menueItems : [NormalCell] = [NormalCell(title: "QUIZ", description: "How much do you know about Co2?")];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initVariables()
        initUI()
     }

    
    func initVariables() {
        collectionItems = [CollectionViewItem(id: "co2byTransport"), CollectionViewItem(id: "co2Today"), CollectionViewItem(id: "totalCo2"),]
    }
    
    func initUI() {
        initHeaderUi()
        initElementUi()
    }
    
    func initHeaderUi() {
        
        self.headerView = GradientView(view: self.headerView).withGradient()
        
    }
    
    func initElementUi() {
        self.addB.layer.masksToBounds = false
        self.addB.layer.cornerRadius = self.addB.frame.width/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.chartCollectionView.reloadData()
    }
    

    
     //MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        if let viewWithTag = self.view.viewWithTag(indexPath.row+100) {
            viewWithTag.removeFromSuperview()
        }
        
        cell.headerLabel.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: 25)
        cell.subCell.frame = CGRect(x: 0, y: 30, width: cell.frame.width, height: cell.frame.height-40)
        
        cell.subCell.backgroundColor = .white
        cell.subCell.layer.cornerRadius = 10
        cell.subCell.layer.masksToBounds = false
        
        
        let chartFrame = CGRect(x: 20, y:0, width:cell.frame.width-40, height:cell.frame.height-50)
        let from = Calendar.current.date(byAdding: .month, value: -1, to: Date())!
        let to = Date()
 
        let collectionViewItem = collectionItems[indexPath.row]
        let chart = collectionViewItem.getChart(frame: chartFrame, from: from, to: to)
        cell.headerLabel.text = collectionViewItem.getTitle()
        
        cell.mainTitle.text = String(format: "%i", Int(chart.totalYPointsValue()));
        cell.subTitle.text = "Co2"
        let chartView = chart.render()
        chartView.tag = indexPath.row+100
        cell.subCell.addSubview(chartView)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        let heightPerItem = collectionView.frame.height - sectionInsets.top - sectionInsets.bottom
        
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    


    @IBAction func add(_ sender: Any) {
        self.performSegue(withIdentifier: "add", sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menueItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        return menueItems[indexPath.row].getCell(cell: cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier = menueItems[indexPath.row].segueIdentifier;
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

