//
//  travelOption.swift
//  Co2
//
//  Created by Tobias Wittekindt on 17.04.19.
//  Copyright © 2019 Ottisoftware. All rights reserved.
//

import UIKit

class TransportOption: NSObject {
    var name : String!
    var isBad : Bool!
    var distance : Double!
    var co2perKm : Double!
    var icon : String!
    var image : String!
    var totalCo2 : Double!
    var created : Date!
    
    init (name: String) {
        self.name = name
        self.created = Date()
    }
    
    func getIcon() -> String {
        return self.icon
    }
    
    func getName() -> String {
        return self.name
    }

    //Returns the total co2 impact. Can be positive -20 (Walking) or negative +20 (plane)
    func getCo2Impact() -> Double {
        let total = getTotalCo2()
        if(!self.isBad){
            return 0 - total
        }
        return total
    }
    
    func getTotalCo2() -> Double {
        self.totalCo2 = self.co2perKm * self.distance
        return self.totalCo2
    }
    
    func getImage() -> UIImage? {
        if(self.image == nil || self.image == ""){
            return UIImage.init(named: "white")
        }
        return UIImage.init(named: self.image)
    }
    
    func getCreateDate() -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        return format.string(from: self.created)
    }
    
    // Returns a Transport item we use this to store it in to the real db
    func getItem() -> TransportItem {
        let item = TransportItem()
        item.name = self.name
        item.distance = self.distance
        item.isBad = self.isBad
        item.co2perKm = self.co2perKm
        item.icon = self.icon
        item.image = self.image
        item.totalCo2 = self.getTotalCo2()
        item.created = self.created
        
        return item
    }
    
    static func initWithItem(item: TransportItem) -> TransportOption {
        let tOption = TransportOption(name: item.name)
        tOption.distance = item.distance
        tOption.isBad = item.isBad
        tOption.co2perKm = item.co2perKm
        tOption.icon = item.icon
        tOption.image = item.image
        tOption.totalCo2 = item.totalCo2
        tOption.created = item.created
        return tOption
    }
    
}
